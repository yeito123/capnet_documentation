# Guia para instalar el nuevo organigrama

## Contenido
>  
>1. Introduccion.
>2. Instalacion.
>3. Configuracion.
>4. Notas Importantes.
>5. Posibles Errores.
>6. Contacto.

### 1. INTRODUCCION
  
Organigrama es un Plug-In que se encarga en gestionar y proyectar a los clientes  
Su organizacion en el taller en cuanto a **Colaboradores** en taller.
  
### 2. INSTALACION

- Verificar que exitan los siguientes archivos:  

 1. ~/OrganigramaConfig.aspx  
 2. ~/OrganigramaConfig.aspx.vb  
 3. ~/tblOrganigrama.aspx  
 4. ~/tblOrganigrama.aspx.vb  
 5. ~/Scripts/organigramaConfig.js  
 6. ~/img/LogoHeader.png  
 7. ~/img/imgOrganigrama/imgFondoOrganigrama.png  
 8. ~/css/stylosOrganigrama.css  
 9. ~/css/StylosRecepcion.css  
 10. ~/Handlers/organigramaConfigHandler.ashx  
 11. ~/Handlers/organigramaConfigHandler.ashx.vb  

- Verificar la existencia de los siguientes objetos en la base de datos de **Tableros**:  
  1. dbo.tb_organigrama:  [Script Tablas Base De datos](https://drive.google.com/file/d/1OyyNUMmKd8N6csGud1DTjS6NT888M3dC/view?usp=sharing "Archivos Requeridos para instalación").
  2. dbo.tb_organigrama: [script actualizacion](https://drive.google.com/drive/u/0/folders/1opNFx98rAFKzM-pB9c5G2kNLumTHW3Y5)

- En la ruta origen de tableros Reemplaze los archivos que encontrara en el siguiente [Link Archivos Organigrama](https://drive.google.com/file/d/1WDUdGHSBT8APkp8_goOmF1TcZ557bzgU/view?usp=sharing "Archivos Requeridos para instalación").
- Verifique que exita la ruta img/imgOrganigrama.

**NOTA: DESCARGAR Y REEMPLAZAR LOS ARCHIVOS DE NUEVO.**

**FELICITACIONES!**  
En este punto ya tienes instalado el PLUG-IN  de organigrama ahora sigue la configuración.

### 3. CONFIGURACION

1. Lo primero es hacer Los menus Accesibles para esto requerimos dirigirnos al sitio de **tableros>permisos>objetos**
   ![tableros>permisos>objetos.](organigrama/img/objetos.png "img.")
En la tabla de abajo encontraras el numero que iria en **objeto**, debes poner el **numero mayor** presente en esta tabla **+ 1**
   ![tableros>permisos>objetos.](organigrama/img/numero.png "img.")  
Este dato ira en el apartado objeto que se ve en la imagen anterior.  
En el campo **Descripcion** pondremos el nombre que utilizaremos para el objeto en el menu.  
Para el ejemplo yo usare 'Organigrama Config' y mi objeto sera '208'  
![tableros>permisos>objetos.](organigrama/img/InsertObjetoPrueba.png "img.")
Una vez hecho esto ya podremos dar click en **'Agregar Objeto'**  
Si todo salio bien en la parte inferior de la pantalla nos aparecera nuestro nuevo objeto!
![tableros>permisos>objetos.](organigrama/img/objetoFirstInsert.png "img.")
**Ya tenemos nuestro objeto creado.**
2. Ahora tendremos que editar nuestro objeto creado en el paso anterior para que redireccione a la pagina que deseeamos y para meterlo en el menu que deseemos haciendo click en el icono señalado en la siguiente imagen.
![tableros>permisos>objetos.](organigrama/img/objetoEdit.png "img.")
Ponemos el nombre del archivo aspx correspondiente al objeto que estamos creando y el menu en el que queremos que este, para este ejemplo usare Citas la informacion de cualquiera de los menus existentes la puedes extraer de cualquiera de los otros registros preexistentes en esta misma pantalla.
![tableros>permisos>objetos.](organigrama/img/objetoModificado.png "img.")
Y damos Click en el icono verde
![tableros>permisos>objetos.](organigrama/img/objetoModificado1.png "img.")
**LISTO!** ya tenemos nuestro objeto creado y configurado, **IMPORTANTE! ahora tendras que darle permisos de acceso al usuario administrador para que pueda tener VISIBLE este objeto en su menu y una vez hecho esto tendras que salirte por completo de tableros y volver a ingresar para que la api tome los cambios en la base de datos.**
3. una vez tengamos el objeto creado y con los permisos debidos establecidos.
![tableros>permisos>objetos.](organigrama/img/objetoCreado.png "img.")
Deberiamos porder acceder a la pantalla mostrada a continuación.
![tableros>permisos>objetos.](organigrama/img/organigramaPrimerIngreso.png "img.")
En esta pantalla es donde haremos las configuraciones y/o modificaciones necesarios a los **Colaboradores** podremos:

**NOTA: A PARTIR DE ESTE MOMENTO USAREMOS LA PALABRA COLABORADOR  PARA REFERIRNOS A TODA AQUELLA PERSONA QUE REQUIERA ESTAR O ESTE EN EL ORGANIGRAMA.**

>* Agregar Colaborador.
>* Cambiar el fondo del organigrama.
>* Actualizar los datos de los Colaboradores.
>* Eliminar Colaborador
>
>### **COLABORADOR**
>
>Un Colaborador esta compuesto por:
>
>* **Id =** Número Asignado por el sistema en el momento de su inserción.
>* **nombre =** nombre del colaborador.
>* **cargo =** el cargo del colaborador.
>* **posNombreX =** la posición en % sobre el eje x para el nombre del colaborador.
>* **posNombreY =** la posición en % sobre el eje y para el nombre del colaborador.
>* **posCargoX =** la posición en % sobre el eje x para el cargo del colaborador.
>* **posCargoY =** la posición en % sobre el eje y para el cargo del colaborador.
>* **imgNombre =** nombre de la imagen (CualquierNombre.png).
>* **imgUrl =** ruta física para la img (estas deben ser para todas la siguiente. >* ~/img/imgOrganigrama/).
>* **posImgX =** la posición en % sobre el eje x para la imagen del colaborador.
>* **posImgY =** la posición en % sobre el eje y para la imagen del colaborador.
>* **pagina =** la pagina en la que ira proyectado el colaborador.
>
>### **AGREGAR**
>![tableros>permisos>objetos.](organigrama/img/AgregarInicio.png "img.")
>**NOTA: TODOS LOS CAMPOS SON OBLIGATORIOS.**
>Se diligencia el formulario y se selecciona la imgane que correspondera al >colaborador.
>![tableros>permisos>objetos.](organigrama/img/agregarLleno.png "img.")
>Si todo esta diligenciado correctamente deberia verse el nuevo registro para el organigrama en la parte de abajo.
>![tableros>permisos>objetos.](organigrama/img/ingresoCompleto.png "img.")
>Apartir de este momento es tan facil como modificar los valores de la fila y la columna correspondientes al colaborador deseado y darle click en ctualizar o si por lo contrario quieres eliminarlo solo deberas darle click en Del y confirmar
>![tableros>permisos>objetos.](organigrama/img/organigramaPrueba.png "img.")
>### **ACTUALIZAR Y/O ELIMINAR**
>Para el ejemplo yo modificare las columnas 'posNombreY' y 'posCargoY'
>![tableros>permisos>objetos.](organigrama/img/organigramaUpdate.png "img.")
>Como podemos ver en la siguiente imagen el nombre y el cargo se han movido respectivamente con el dato que hemos actualizado en las columnas antes mencionadas.
>![tableros>permisos>objetos.](organigrama/img/organigramaUpdateFin.png "img.")
para eliminar hacemos click en el boton Del del registro a eliminar y confirmamos. **ESTA ACCÓN ES IRREVERSIBLE!**
>![tableros>permisos>objetos.](organigrama/img/organigramaDelete.png "img.")
>Una vez confirmemos el registro sera eliminado y la pantalla cargará
>![tableros>permisos>objetos.](organigrama/img/organigramaDeleteFin.png "img.")
>**CAMBIAR FONDO**
>
>![tableros>permisos>objetos.](organigrama/img/fondo.png "img.")
Facil! seleccionamos una nueva imagen y hacemos click en **'Cambiar'**
Esto lo que hará sera reemplazar o crear la imagen de fondo si esta no existe.  
**NOTA: EL TAMAÑO DE LA IMAGEN PARA EL FONDO DEBE SER DE ALMENOS 1400PX x 600PX Y DEBE ESTAR EN HD Y NO PESAR MAS DE 1MB**
>![tableros>permisos>objetos.](organigrama/img/fondoIni.png "img.")
>![tableros>permisos>objetos.](organigrama/img/fondoFin.png "img.")

En este punto ya tenemos instalado y configurado nuestro organigrama ya casi podremos dejarlo configurado en el cliente!

4. Otras Configuraciones:
>* Estilos y Fuentes: Para modificar los estilos y las fuentes que son visibles para los clientes se estas deberan ser modificadas en el archivo **~/css/stylosOrganigrama.css**  
>Los estilos Son los siguientes (lblTitulo, lblNombreEmpleado, lblCargoEmpleado, imgEmpleado)  
>- lbltitulo
>![tableros>permisos>objetos.](organigrama/img/lblTitulo.png "img.")
>- lblNombreEmpleado
>![tableros>permisos>objetos.](organigrama/img/lblNombreEmpleado.png "img.")
>- lblCargoEmpleado
>![tableros>permisos>objetos.](organigrama/img/lblCargoEmpleado.png "img.")  
>- imgEmpleado
>![tableros>permisos>objetos.](organigrama/img/imgEmpleado.png "img.")

5. Otras Cosas que deben existir:
>
>* ~/img/imgLogomazda.png
>* Parametro en sccParametros correspondiente al pie de pagina del organigrama:
>![tableros>permisos>objetos.](organigrama/img/parametro.png "img.")
>![tableros>permisos>objetos.](organigrama/img/parametroFin.png "img.")
>* control en web.config '
>![tableros>permisos>objetos.](organigrama/img/controlImagenes.png "img.")
Esta Corresponde a la ruta fisica donde estaran las imagenes de organigrama esta ruta debe ser la **ruta fisica de tableros + "\img\imgOrganigrama\\"


**FELICITACIONES! ya tienes tu plug-in instalado y configurado. ahora debes configurarlo en el tv del cliente recuerda que este tv debe ser SMART TV y si este mismo ejecuta javascript sería mucho mejor!!!**

### 4. NOTAS IMPORTANTES

>>* NOTA: PARA QUE ORGANIGRAMA.ASPX FUNCIONE CORRECTAMENTE DEBE EXISTIR AL MENOS UN REGISTRO EN TB_ORGANIGRAMA
>>* EL FONDO DE LAS PAGINAS DEBERA ESTAR EN LA RUTA ~/img/imgOrganigrama/ Y DEBERA LLAMARSE imgFondoOrganigrama.png
>>* TODAS LAS IMAGENES QUE ESTARAN EN EL ORGANIGRAMA DEBERAN ESTAR ALMACENADAAS EN ~/img/imgOrganigrama/ Y TODAS DEBERAN SER .png
>>* TODAS LAS POSICIONES DEBERA SER DADAS EN PORCENTAJES EJEM (posNombreX = 10) pueden ser decimales
>>* las páginas deben ser CONSECUTIVAS!!! no puede pasar de la pagina 1 a la 3 por ejemplo deben ser 1,2,3 etc..
>>* TODOS LOS REGISTROS TIENEN QUE TENER UNA PAGINA Y SUS RESPECTIVAS POSICIONES, EL NO PRESENTARSE ESTA CONDICION HARA QUE EL SISTEMA TRUENE.
>>* **ESTA ACTUALIZACION SERA UTIL UNICAMENTE EN TABLEROS MAZDA COLOMBIA.   SI REQUIEREN QUE SE LIBERE PARA OTRAS VERSION POR FABOR SOLICITARLO CON ANTELACION Y EN UNA INCIDENCIA APARTE. HACIENDO REFERENCIA A ESA DOCUMENTACION.**




### 5. POSIBLES ERRORES  

>* **001:** Este error se presenta cuando se ha actualizados los archivos pero no la base de datos.  Llevando a que la API quiera consultar un objeto que no existe. para solucionarlo favor remitirse al punto que hace referencia  la base de datos en esta misma guia.
>![001: Referencia a objeto no establecida como instancia a un objeto.](organigrama/img/Error001.png "Error: 001.")  
Si el paso anterio no Soluciono el problema Verifique que El organigrama ya cuente con una confgiuracion inicial.


### 6. CONTACTO
> Para cualquier Duda o inquitud ponerse en contacto con el siguiente correo yeison.torres@capitalnetwork.com.mx





