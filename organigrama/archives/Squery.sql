--1. SE CREA LA TABLA TB_ORGANIGRAMA
/****** Object:  Table [dbo].[tb_organigrama]    Script Date: 9/21/2020 6:28:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_organigrama](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](200) NOT NULL,
	[cargo] [varchar](200) NOT NULL,
	[posNombreX] [float] NULL,
	[posNombreY] [float] NULL,
	[posCargoX] [float] NULL,
	[posCargoY] [float] NULL,
	[imgNombre] [varchar](200) NOT NULL,
	[imgUrl] [varchar](200) NOT NULL,
	[posImgX] [float] NULL,
	[posImgY] [float] NULL,
	[pagina] [int] NOT NULL,
 CONSTRAINT [PK_tb_organigrama] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
-- FIN PASO 1